#! /bin/bash
trap 'kill $(jobs -p)' EXIT
mkdir $4
for ((i=$1; i<$[$2+1]; i++))
do
echo $i
bash testbench.sh $3 $i $4/$i 
done
