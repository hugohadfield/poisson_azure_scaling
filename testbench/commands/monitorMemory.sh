#! /bin/bash
printf "Memory\t\tCPU\n"
end=$((SECONDS+36000))
while [ $SECONDS -lt $end ]; do
MEMORY=$(free -m | awk 'NR==2{printf "%.2f\t\t", $3 }')
CPU=$(top -bn1 | grep load | awk '{printf "%.2f%%\t\t\n", $(NF-2)}')
MILLISECS=$(date +"%T.%3N")
echo "$MEMORY$CPU$MILLISECS"
sleep $1
done
