#! /bin/bash
trap 'kill $(jobs -p)' EXIT
mkdir $(pwd)/$3	
bash monitorMemory.sh 0.1 >> $(pwd)/$3/memdata.txt &
mpirun -np $2 $1
kill $(jobs -p)
cp ../data/$(ls -t ../data | head -n1) $(pwd)/$3