
import matplotlib.pyplot as plt
import sys
import math
from datetime import datetime

from mpltools import annotation



# solve for a and b
def best_fit(X, Y):

    xbar = sum(X)/len(X)
    ybar = sum(Y)/len(Y)
    n = len(X) # or len(Y)

    numer = sum([xi*yi for xi,yi in zip(X, Y)]) - n * xbar * ybar
    denum = sum([xi**2 for xi in X]) - n * xbar**2

    b = numer / denum
    a = ybar - b * xbar

    print('best fit line:\ny = {:.2f} + {:.2f}x'.format(a, b))

    return a, b

def add_slope_marker(X,a,b):
    yfit = [a + b * xi for xi in X]
    x_pos = (max(X)+min(X) ) /2
    y_pos = a + b*x_pos 
    annotation.slope_marker((x_pos,y_pos) , (round(b,2), 1))


time_array = []
cores = []
for filenumber,file_name in enumerate(sys.argv[1:]):
    n = 0
    time_array.append([])
    with open(file_name) as f_obj:
        split_name = file_name.split('/')
        cores.append(float(split_name[-2]))
        for line in f_obj.readlines():
            if n != 0:
                stripped_data = line.replace("%", "")
                split_line = stripped_data.split()
                utc_time = datetime.strptime(split_line[2],
                                 "%H:%M:%S.%f")
                time_array[filenumber].append(utc_time)
            else:
                n = 1
print(cores)
time_taken = [(t[-1] - t[0]).seconds for t in time_array ]
log_cores = [math.log(c) for c in cores]
log_time = [math.log(t) for t in time_taken]
plt.plot(log_cores,log_time,'+')
a,b = best_fit(log_cores[:3], log_time[:3])
yfit = [a + b * xi for xi in log_cores]
plt.plot(log_cores, yfit)
add_slope_marker(log_cores[:3],a,b)

plt.ylabel('Log compute time log(s)')
plt.xlabel('Log number of cores')
plt.savefig('timecores_fig.png')
plt.show()
