#!/usr/bin/env bash
arr=()
for f in $1/*; do
    if [ -d ${f} ]; then
        # Will not run if no directories are available
        echo $f
        arr+=($f/memdata.txt)
    fi
done
python plotCoresToTime.py ${arr[@]}
mv timecores_fig.png $2

