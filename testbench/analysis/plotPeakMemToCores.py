
import matplotlib.pyplot as plt
import sys
from datetime import datetime

from mpltools import annotation



# solve for a and b
def best_fit(X, Y):

    xbar = sum(X)/len(X)
    ybar = sum(Y)/len(Y)
    n = len(X) # or len(Y)

    numer = sum([xi*yi for xi,yi in zip(X, Y)]) - n * xbar * ybar
    denum = sum([xi**2 for xi in X]) - n * xbar**2

    b = numer / denum
    a = ybar - b * xbar

    print('best fit line:\ny = {:.2f} + {:.2f}x'.format(a, b))

    return a, b

def add_slope_marker(X,a,b):
    yfit = [a + b * xi for xi in X]
    x_pos = (max(X)+min(X) ) /2
    y_pos = a + b*x_pos 
    annotation.slope_marker((x_pos,y_pos) , (round(b,2), 1))


mem_array = []
cores = []
for filenumber,file_name in enumerate(sys.argv[1:]):
    n = 0
    mem_array.append([])
    with open(file_name) as f_obj:
        split_name = file_name.split('/')
        cores.append(float(split_name[-2]))
        for line in f_obj.readlines():
            if n != 0:
                stripped_data = line.replace("%", "")
                split_line = stripped_data.split()
                mem_array[filenumber].append(float(split_line[0]))
            else:
                n = 1
print(cores)
peak_mem = [max(m) for m in mem_array]
plt.scatter(cores,peak_mem)
a, b = best_fit(cores, peak_mem)
yfit = [a + b * xi for xi in cores]
#plt.plot(cores, yfit)
#add_slope_marker(cores,a,b)

plt.ylabel('Peak memory usage (MB)')
plt.xlabel('Number of cores')
plt.savefig('peakmemcores_fig.png')
plt.show()
