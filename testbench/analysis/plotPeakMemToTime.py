import matplotlib.pyplot as plt
import sys
from datetime import datetime

mem_array = []
time_array = []
for filenumber,file_name in enumerate(sys.argv[1:]):
    n = 0
    mem_array.append([])
    time_array.append([])
    with open(file_name) as f_obj:
        for line in f_obj.readlines():
            if n != 0:
                stripped_data = line.replace("%", "")
                split_line = stripped_data.split()
                mem_array[filenumber].append(split_line[0])
                utc_time = datetime.strptime(split_line[2],
                                 "%H:%M:%S.%f")
                time_array[filenumber].append(utc_time)
            else:
                n = 1

time_taken = [(t[-1] - t[0]).seconds for t in time_array ]
print(time_taken)
peak_mem = [max(m) for m in mem_array]
print(peak_mem)

#fig, ax = plt.subplots()
#ax.set_xscale('log', basex=2)
#ax.set_yscale('log', basey=2)
plt.scatter(time_taken,peak_mem, s=20)
plt.ylabel('Peak memory usage (MB)')
plt.xlabel('Compute time (s)')
plt.savefig('peakmemtime_fig.png')
plt.show()
