import matplotlib.pyplot as plt
import sys
from datetime import datetime

mem_array = []
time_array = []
cores = []
for filenumber,file_name in enumerate(sys.argv[1:]):
    n = 0
    mem_array.append([])
    time_array.append([])
    with open(file_name) as f_obj:
        split_name = file_name.split('/')
        cores.append(split_name[-2] + " cores")
        for line in f_obj.readlines():
            if n != 0:
                stripped_data = line.replace("%", "")
                split_line = stripped_data.split()
                mem_array[filenumber].append(split_line[0])
                utc_time = datetime.strptime(split_line[2],
                                 "%H:%M:%S.%f")
                time_array[filenumber].append(utc_time)
            else:
                n = 1
    secondsSinceStart = [(time_array[filenumber][i] - time_array[0][0]).total_seconds() for i in range(len(time_array[filenumber]))]
    plt.plot(secondsSinceStart, mem_array[filenumber])
plt.legend(cores, loc='upper left')
plt.ylabel('Total memory usage (MB)')
plt.xlabel('Time (s)')
plt.savefig('sweep_fig.png')
plt.show()
