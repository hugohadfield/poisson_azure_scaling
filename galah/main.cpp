// Copyright (C) 2017 Ettie Unwin and Nathan Sime
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-04-28
// Last changed: 2017-04-28

#include "solver/PoissonSolver.h"
#include <pods.h>
#include <dolfin.h>
#include <iostream>
#include <fstream>
#include <mpi.h>
#include <memory>

using namespace dolfin;
using namespace pods;

int main()
{
 // Sets logger levels
  set_pods_log_level(50);
  set_log_level(50);
  
  // Sets variables for MC/MLMC
  int n_cells_L0 =8;
  double tol = 1e-1;
  int n_samples = 10;
  int L = 4;

  // Sets information about random fields
  double seed = 10;
  double mean_density = 8000.0;

  // Sets save information
  std::string save_path = "../data/";
  std::string solver_type = "poisson";
  std::string statistical_programme = "mlmc";  

  // Generate solver
  std::vector<QOI> QOIholder;
  std::shared_ptr<GenericSolver> solver_class;
  
  solver_class = std::make_shared<PoissonSolver>(n_cells_L0,
						 mean_density);

  // Runs mc/ mlmc routine
  if (statistical_programme == "mlmc")
  {
    MLMC mlmc = MLMC(solver_class, save_path, tol, 10);
    QOIholder = mlmc.routine(seed);
  }
  else
  {
    MC mc = MC(solver_class, save_path, L, tol);
    QOIholder = mc.routine(seed);
  }

  // Prints quantities of interest to std::cout
  for (std::size_t j=0; j<QOIholder.size(); ++j)
  {
    std::cout << "Expectation of u(0.5, 0.5, 0.5) " <<  ": "
	      << QOIholder[j].expectation << std::endl;

    std::cout << "Variance of u(0.5, 0.5, 0.5) " << ": "
	      << QOIholder[j].variance << std::endl;
  }
  SubSystemsManager::finalize();
  MPI_Finalize();
}
