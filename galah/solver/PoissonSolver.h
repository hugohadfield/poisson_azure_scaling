// Copyright (C) 2017 Ettie Unwin and Nathan Sime
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-04-28
// Last changed: 2017-04-28

#ifndef POISSONSOLVER_H
#define POISSONSOLVER_H
#include <pods.h>
#include <dolfin.h>
#include <memory>

using namespace pods;

class PoissonSolver: public GenericSolver
/// This is an example Poisson Solver under uncertain forcing, f.
/// f is generated using LP smoothing.
/// -grad(u(x, \omega))=f(x, \omega) in D
/// u(x, \omega) = 0 on \Gamma
{
public:
  // Constructor
  PoissonSolver(int n_cells_L0, double mean);

  // Solves poisson
  std::vector<double> solver(std::shared_ptr<const dolfin::Mesh> mesh,
			     double seed,
			     std::shared_ptr<const dolfin::Mesh> random_field_mesh)
    override;

 // Generates initial mesh
  std::shared_ptr<dolfin::Mesh> get_initial_mesh(MPI_Comm comm) override
  {
    return std::make_shared<dolfin::UnitCubeMesh>(comm, _n_cells_L0,
						    _n_cells_L0, _n_cells_L0);
  }

  // Returns solver info
  solver_info get_solver_info() override;

private:
  int _n_cells_L0;
  double _mean, _lp_smoother_mu, _lp_smoother_sigma;

};
#endif
