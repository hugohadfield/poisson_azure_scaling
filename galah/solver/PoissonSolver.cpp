// Copyright (C) 2017 Ettie Unwin and Nathan Sime
//
// This file is part of PODS.
//
// PODS is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// PODS is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with PODS. If not, see <http://www.gnu.org/licenses/>.
//
// The logger is based on the FEniCS libraries logger, see
// <https://fenicsproject.org>.
//
// First added:  2017-04-28
// Last changed: 2017-04-28

#include <dolfin.h>
#include <pods.h>
#include "Poisson.h"
#include "PoissonSolver.h"

using namespace dolfin;
using namespace pods;

//-----------------------------------------------------------------------------
class DirichletBoundary : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  {
    return on_boundary;
  }
};
//-----------------------------------------------------------------------------
PoissonSolver::PoissonSolver(int n_cells_L0, double mean)
  : _n_cells_L0(n_cells_L0), _mean(mean)
{
  double lp_smoother_mu = 0.0;
  double lp_smoother_sigma = 10000.0;
  _lp_smoother_mu = lp_smoother_mu;
  _lp_smoother_sigma = lp_smoother_sigma;
}
//-----------------------------------------------------------------------------
std::vector<double> PoissonSolver::solver(std::shared_ptr<const Mesh> mesh,
					  double seed,
					  std::shared_ptr<const Mesh> random_field_mesh)
{
  MPI_Comm mesh_mpi_comm = mesh->mpi_comm();
  parameters["mesh_partitioner"] = "ParMETIS";

  // Create mesh and function space
  auto V = std::make_shared<Poisson::FunctionSpace>(mesh);

  // Define boundary condition
  auto u0 = std::make_shared<Constant>(0.0);
  auto boundary = std::make_shared<DirichletBoundary>();
  DirichletBC bc(V, u0, boundary);

  // Define variational forms
  Poisson::BilinearForm a(V, V);
  Poisson::LinearForm L(V);

  ProbabilitySpace omega = ProbabilitySpace("normal", seed);
  LpSmoothed function_generator(mesh, random_field_mesh, omega, _lp_smoother_mu,
				_lp_smoother_sigma);
  L.f = function_generator.sample();

  // Compute solution
  Function u(V);
  solve(a == L, u, bc);

  u.set_allow_extrapolation(true);

  return {u(0.5, 0.5, 0.5)};
}
//----------------------------------------------------------------------------- 
solver_info PoissonSolver::get_solver_info()
{
  solver_info info;
  info.problem = "poisson";
  info.random_field = "LPSmoother: mu " + std::to_string(_lp_smoother_mu)
    + " sigma " + std::to_string(_lp_smoother_sigma);
  info.qoi = {"u(0.5 0.5, 0.5)"};
  info.mesh = std::to_string(_n_cells_L0);

  return info;
}
//----------------------------------------------------------------------------- 
