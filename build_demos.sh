for entry in "$(pwd)"/*
do
  if [ -d "$entry" ]; then
  	echo "$entry"
    cd "$entry"
    if [ -d solver ]; then
    cd solver
    for uflfile in *.ufl
    do
        echo "$uflfile"
        ffc -l dolfin "$uflfile"
    done
    cd ../
    fi
  mkdir build
  cd build
  cmake ..
  make
  cd ../../
  fi
done